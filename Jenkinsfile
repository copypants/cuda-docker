node {
  stage('Prepare') {
    cleanWs()
    checkout scm
    withCredentials([
      usernamePassword(credentialsId: 'copypants.azurecr.io', usernameVariable: 'DOCKER_USER', passwordVariable: 'DOCKER_PASS')
    ]) {
      sh """
        set -e

        docker login copypants.azurecr.io -u "${DOCKER_USER}" -p "${DOCKER_PASS}"
      """
    }
  }
  stage('Build Base') {
    sh """
      set -e

      docker build --pull \
        -f Dockerfile.9.1-base-stretch \
        -t copypants.azurecr.io/cuda:9.1-base-stretch .
    """
  }
  stage('Build Runtime') {
    sh """
      set -e
      
      docker build \
        -f Dockerfile.9.1-runtime-stretch \
        -t copypants.azurecr.io/cuda:9.1-runtime-stretch .
    """
  }
  stage('Build CUDNN Runtime') {
    sh """
      set -e

      docker build \
        -f Dockerfile.9.1-cudnn7-runtime-stretch \
        -t copypants.azurecr.io/cuda:9.1-cudnn7-runtime-stretch .
    """
  }
  stage('Build Devel') {
    sh """
      set -e

      docker build \
        -f Dockerfile.9.1-devel-stretch \
        -t copypants.azurecr.io/cuda:9.1-devel-stretch .
    """
  }
  stage('Build CUDNN Devel') {
    sh """
      set -e

      docker build \
        -f Dockerfile.9.1-cudnn7-devel-stretch \
        -t copypants.azurecr.io/cuda:9.1-cudnn7-devel-stretch .
    """
  }
  stage('Publish') {
    sh """
      set -e

      docker push copypants.azurecr.io/cuda:9.1-base-stretch
      docker push copypants.azurecr.io/cuda:9.1-runtime-stretch
      docker push copypants.azurecr.io/cuda:9.1-cudnn7-runtime-stretch
      docker push copypants.azurecr.io/cuda:9.1-devel-stretch
      docker push copypants.azurecr.io/cuda:9.1-cudnn7-devel-stretch            
    """
  }
}
