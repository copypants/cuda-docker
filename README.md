Basically copied from https://hub.docker.com/r/nvidia/cuda/

## CUDA 9.1
- [`9.1-base-stretch` (*Dockerfile.cuda-9.1-base-stretch*)](https://bitbucket.org/copypants/cuda-docker/src/master/Dockerfile.9.1-base-stretch)
- [`9.1-runtime-stretch` (*Dockerfile.cuda-9.1-runtime-stretch*)](https://bitbucket.org/copypants/cuda-docker/src/master/Dockerfile.9.1-runtime-stretch)
- [`9.1-devel-stretch` (*Dockerfile.cuda-9.1-devel-stretch*)](https://bitbucket.org/copypants/cuda-docker/src/master/Dockerfile.9.1-devel-stretch)
- [`9.1-cudnn7-runtime-stretch` (*Dockerfile.cudnn-9.1-runtime-stretch*)](https://bitbucket.org/copypants/cuda-docker/src/master/Dockerfile.cudnn-9.1-runtime-stretch)
- [`9.1-cudnn7-devel-stretch` (*Dockerfile.cudnn-9.1-devel-stretch*)](https://bitbucket.org/copypants/cuda-docker/src/master/Dockerfile.cudnn-9.1-devel-stretch)